# TKO_2111-Yatzy

Python Yatzy-peli. 


Suorita python-skripti:
Avaa jokin terminal-sovellus (git bash, cmd, yms.) ja anna komento: python main.py
Pelin voi lopettaa sen alettua komennolla: quit

Huom! Peli sisältää merkkejä, joita ei ole välttämättä terminal sovelluksen character settiin asetettu.
Locale asetuksista voi muuttaa locale C ja character set ISO-8859-10 (Nordic). Esim. git bash, 
yläpalkin logosta oikealla hiiren napilla klikkaamalla -> options -> text -> locale: C, character set ISO-8859-10.



Valtteri Holma, Mikko Simola
University of Turku, 15/12/2022
