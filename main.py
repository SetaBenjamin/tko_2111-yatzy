import json
import random

# Globaalit muuttujat.
rounds = 0
playerCount = 1
current_player = 0
playerNames = []

# Luokka tiedostojen käsittelyyn.
class DataHandler():
    @classmethod
    def read(cls, path:str, key:str=None):
        try:
            with open(path, 'r') as file:
                jsonData = json.load(file)
                return jsonData[key] if key else jsonData
        except:
            return print(errorSet.get("fileError"))
    
    @classmethod
    def write(cls, path:str, data:dict):
        try:
            with open(path, 'w') as file:
                json.dump(data, file, indent=4)
        except:
            return print(errorSet.get("fileError"))

# Tiedostopolut.
paths = {
    'scores': "scores.json", 
    'res': "resources.json"
    }

# Virheilmoitukset.
errorSet = {
    "invalidInput": "Virheellinen syöte!",
    "invalidInputType": "Virheellinen syötteen tyyppi!",
    "invalidCommand": "Tuntematon komento!", 
    "invalidIndex": "Virheellinen indeksi!",
    "fileError": "Virhe tiedoston käsittelyssä!",
    }

# Apufunktio muuttamaan syötteen muotoon int.
def toInt(value):
    try:
        return int(value)
    except:
        return print(errorSet.get("invalidInputType"))

# Tallenna pelaajien tiedot pelin alussa.
def save_players():
    while True:
        player_amount = toInt(input("Pelaajien määrä? [1-6] "))

        if type(player_amount) != int:
            continue
        
        if 1 <= player_amount <= 6:
            break
        print(errorSet.get("invalidInput"))
        print("Pelaajien määrän tulee olla välillä 1-6.\n")
    
    # Tallenna pelaajille tyhjät pöytäkirjat.
    player_list = []
    for o in range(player_amount):
        name = input(f'Anna nimi P{o+1}: ')
        if name == "":
            name = f'P{o+1}'
        player_list.append(name)
    
    global playerCount
    playerCount = player_amount

    global playerNames
    playerNames = player_list
    
    emptyList = DataHandler.read(paths["res"], "emptyList")
    
    d = {"scores":[
        {
            'name': player, 
            'table': emptyList,
            'upperSum': 0,
            'total': 0
            } for player in player_list]
        }
    
    DataHandler.write(paths["scores"], d)
    return

# Tallentaa pelaajan valitseman yhdistelmän pöytäkirjaan.
def saveScore(rolls:list):
    scores = DataHandler.read(paths["scores"], "scores")
    d = {"scores": scores}

    while True:

        # Mihin pöytäkirjan indeksiin tallennetaan.
        indeksi = toInt(input("Mihin haluat tallentaa? 1-15 "))

        if type(indeksi) != int:
            continue
        
        if not 1 <= int(indeksi) <= 15:
            print(errorSet.get("invalidIndex"))
            continue
        
        # Tarkistetaan pelaajan valitseman yhdistelmän antamat pisteet.
        points = check_combination(rolls, indeksi)

        idx = indeksi - 1 if indeksi <= 6 else indeksi - 7
        # Tallennetaan pisteet pöytäkirjaan.
        if indeksi <= 6:
            # Yläosan yhdistelmät.
            if scores[current_player]["table"]["upper"][idx] != "-": 
                print(errorSet.get("invalidIndex"))
                print("Et voi tallentaa samaan kohtaan useamman kerran.")
                continue
            scores[current_player]["table"]["upper"][idx] = points
            scores[current_player]["upperSum"] += points
            break
        else:
            # Alaosan yhdistelmät.
            if scores[current_player]["table"]["lower"][idx] != "-":
                print(errorSet.get("invalidIndex"))
                print("Et voi tallentaa samaan kohtaan useamman kerran.")
                continue
            scores[current_player]["table"]["lower"][idx] = points
            break
    
    # Jos ylätason pisteet ylittää 63 pistettä, pelaaja saa 50 bonuspistettä.
    if rounds == 14 and scores[current_player]["upperSum"] > 63:
        print("Sait yläosan yhdistelmistä yli 63 pistettä, joten saat 50 bonuspistettä. ")
        points += 50

    # Päivitetään pelaajan kokonaispisteet.
    scores[current_player]["total"] += points

    DataHandler.write(paths["scores"], d)
    return 

# Laske samanlaisten noppien silmälukujen määrä.
def countOccurances(list:list):
    counts = {}
    for l in list:
        if l in counts:
            counts[l] += 1
        else:
            counts[l] = 1
    return counts

# Tarkistaa pelaajan valitseman yhdistelmän ja palauttaa pistemäärän.
def check_combination(rolls : list, combination : int):
    # Laskee noppien silmälukujen määrän.
    counts = countOccurances(rolls)
    points = 0
    
    # Yläosan yhdistelmät.
    if combination in range(1, 7):
        points += combination * counts[combination] if combination in counts else 0

    # Alaosan yhdistelmät.
    pairs = [key for key, value in counts.items() if value >= 2]
    tri = [key for key, value in counts.items() if value >= 3]
    quad = [key for key, value in counts.items() if value >= 4]

    match combination:
        # Yksi pari
        case 7: 
            if len(pairs) >= 1: 
                points += max(pairs) * 2
        
        # Kaksi paria
        case 8:
            if len(pairs) >= 2: 
                points += pairs[0] * 2 + pairs[1] * 2
        
        # Kolme samaa
        case 9: 
            if len(tri) >= 1: 
                points += tri[0] * 3
        
        # Neljä samaa
        case 10:
            if len(quad) >= 1: 
                points += quad[0] * 4
        
        # Pikku suora
        case 11:
            s = [1, 2, 3, 4, 5]
            if sorted(rolls) == s:
                points += 15
        
        # Iso suora
        case 12:
            s = [2, 3, 4, 5, 6]
            if sorted(rolls) == s:
                points += 20
        
        # Täyskäsi
        case 13:
            if len(pairs) >= 1 and len(tri) >= 1: 
                points += (tri[0] * 3) + (pairs[0 if pairs[0] != tri[0] else 1]) * 2
        
        # Sattuma
        case 14:
            nums = [key * value for key, value in counts.items()]
            points += sum(nums)
        
        # Yatzy
        case 15:
            nums = [key for key, value in counts.items() if value >= 5]
            if len(nums) >= 1: 
                points += 50
    
    return points

# Tulostaa pelaajien loppupisteet ja voittajan.
def print_final_score():
    scores = DataHandler.read(paths["scores"], "scores")

    highest_score = 0
    winner = ""
    for pelaaja in range(playerCount):
        print("")
        print(f'Pelaajan {playerNames[pelaaja]} kokonaispisteet: {scores[pelaaja]["total"]}')
        # Päivitetään highes_score ja winner, jos löytyy pelaaja, jonka pisteet ovat suuremmat kuin nykyinen highes_score.
        if scores[pelaaja]["total"] > highest_score:
            highest_score = scores[pelaaja]["total"]
            winner = (playerNames[pelaaja])

    print("")
    print(f'Voittaja on {winner}!!!')

### Commands ###
# Tulosta ohjeet.
def ohje(args:list):
    print(DataHandler.read(paths["res"], "ohje"))
    return

# Heitä noppia.
def roll(args:list):
    # args[0] = dice indecies to roll
    # Muuta args lista int listaksi.
    idx = []
    for a in args:
        if not a.isdigit():
            return print(errorSet.get("invalidInputType"))
        if not int(a) in range(1, 6):
            return print(errorSet.get("invalidIndex"))
        idx.append(int(a))
    
    # Heitä uudet nopat vain, jos lista ei ole tyhjä.
    if not args == [] and not rolls == []:
        newRoll = rolls
        for i in idx:
            newRoll[i-1] = random.randint(1, 6)
    # Jos lista on tyhjä, heitä kaikki nopat.
    else:
        newRoll = [random.randint(1, 6) for _ in range(5)]

    return newRoll

# Päätä vuoro.
def skip(args:list):
    # args = []
    # Palauta pääloopille 0 käskynä lopettaa vuoro tai -1 jos vuoro on ensimmäinen.
    return 0 if turn != 1 else -1

# Printtaa tulostaulu.
def scores():
    table = DataHandler.read(paths["res"], "table")
    scores = DataHandler.read(paths["scores"], "scores")[current_player]
    for rivi in range(len(table["upper"])):
        print(table["upper"][rivi], ":", scores["table"]["upper"][rivi])
        print("-" * 30)

    for rivi in range(len(table["lower"])):
        print(table["lower"][rivi], ":", scores["table"]["lower"][rivi])
        print("-" * 30)
    
    print(f'Pelaajan {scores["name"]} kokonaispisteet: {scores["total"]}')

    return

# Suorita komentoja pelaajan syötteen perusteella.
def commandHandler(args:list):
    commands = {
        "help": ohje,
        "roll": roll,
        "skip": skip,
        "quit": lambda _: quit()
    }
    # Kutsu komentoa jos se on olemassa.
    if len(args) == 0 or not args[0] in commands.keys():
        print(errorSet.get("invalidCommand"))
        return
    return commands[args[0]](args[1:])

# Aloita uusi peli.
def newGame():
    if input("\nPaina 'Enter' aloittaaksesi.\nVoit keskeyttää syöttämällä 'N'\n").upper() == "N":
        quit()
    
    print("Saat ohjeet komennolla \"help\".")
    print("Voit sulkea pelin komennolla \"quit\".")
    save_players()

if __name__ == "__main__":
    newGame()

# Päälooppi.
while True:
    turn = 1
    rolls = []
    print(f'\nVuoro pelaajalla {playerNames[current_player]}\n')
    # Printtaa tulostaulu vuoron alussa.
    scores()
    while turn <= 3:
        # Suorita kunnes vuoro loppuu.
        strRoll = "Heitä nopat: roll < heitettävät nopat > "
        strSkip = "Lopeta vuoro: skip "
        availableOptions = strRoll if turn < 2 else "\n".join([strRoll, strSkip])

        # Kysy pelaajalta komento.
        action = input(f'\n{availableOptions}\n:').lower().split()
        # Suorita komento commandHandlerin kautta ja tallenna return-arvo.
        result = commandHandler(action)
        
        # Jos skip palauttaa 0, lopeta vuoro.
        if result == 0:
            print("Vuoro lopetettu.")
            break
        elif result == -1:
            print(errorSet.get("invalidInput"))
            continue
        
        # Jos roll palauttaa listan, tallenna se. Muuten jatka virheellisen syötteen vuoksi.
        if not type(result) == list:
            continue
        
        # Tallenna noppien heitto.
        rolls = result
        print(f'Heitto {turn}/3')
        print(f'\nHeitit: {result}')

        turn += 1
    
    # Vuoro loppui, tallenna pisteet.
    saveScore(rolls)
    
    # Vaihda pelaajaa ja päivitä pelattujen vuorojen määrä.
    if current_player == playerCount-1:
        rounds += 1
    current_player += 1
    current_player %= playerCount
    
    # Tarkista vuoroista loppuiko peli.
    if rounds == 15:
        print_final_score()
        if input("\n Aloitetaanko uusi peli? (Y/N) ").upper() == "Y":
            rounds = 0
            current_player = 0
            newGame()
            continue
        break
